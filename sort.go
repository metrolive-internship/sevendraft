package main

import (
	"fmt"
)

func sortbubble(n []int) {
	var check bool
	check = true
	for  check {
		check = false
		for i:=1;i< len(n);i++  {
			tmp:=0
			if n[i-1]> n[i]{
				check = true
				tmp=n[i-1]
				n[i-1]=n[i]
				n[i]=tmp
			}
		}
	}

	return
}

func main() {

	// exam  input 30 : 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
	// exam  input 40 : 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0

	fmt.Printf("Input size:")
	var size int
	fmt.Scanf("%d",&size)
	slice := make([]int, size)

	for i:=0;i<size ;i++  {
		fmt.Scanf("%d",&slice[i])
	}


	sortbubble(slice)
	fmt.Print(slice)
}
